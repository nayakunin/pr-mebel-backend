import { Module } from '@nestjs/common';
import { SendEmailModule } from './send-email/send-email.module';

@Module({
    imports: [SendEmailModule],
    controllers: [],
    providers: [],
})
export class AppModule {}
