import { Module } from '@nestjs/common';
import { SendEmailService } from './send-email.service';
import { SendEmailController } from './send-email.controller';
import { MulterModule } from '@nestjs/platform-express';
import { MailerModule } from '@nestjs-modules/mailer';
import { memoryStorage } from 'multer';

@Module({
    imports: [
        MulterModule.register({
            storage: memoryStorage(),
        }),
        MailerModule.forRoot({
            transport: 'smtps://zakaz@pr-mebel.ru:nobiele000@smtp.mail.ru',
        }),
    ],
    controllers: [SendEmailController],
    providers: [SendEmailService],
})
export class SendEmailModule {}
