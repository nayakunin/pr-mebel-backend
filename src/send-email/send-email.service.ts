import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class SendEmailService {
    constructor(private readonly mailerService: MailerService) {}

    send(
        name: string,
        tel: string,
        files: Express.Multer.File[],
        email?: string,
        description?: string,
        meta?: string,
    ) {
        return this.mailerService.sendMail({
            from: 'zakaz@pr-mebel.ru',
            to: 'zakaz@pr-mebel.ru',
            replyTo: email || 'zakaz@pr-mebel.ru',
            subject: `[ТЕСТ] Расчет | ${name} | ${tel}`,
            html: `
            <p><strong>Имя:</strong><br>${name}</p>
            <p><strong>Телефон:</strong><br>${tel}</p>
            ${email ? `<p><strong>Почта:</strong><br>${email}</p>` : ''}
            ${
                description
                    ? `<p><strong>Описание:</strong><br>${description}</p>`
                    : ''
            }
            ${meta ? `<p>${meta}</p>` : ''}
        `,
            attachments: files.map((file) => ({
                filename: file.filename,
                content: file.buffer,
                contentType: file.mimetype,
            })),
        });
    }
}
