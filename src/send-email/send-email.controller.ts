import {
    Body,
    Controller,
    Post,
    UploadedFiles,
    UseInterceptors,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { SendEmailService } from './send-email.service';

@Controller('send-email')
export class SendEmailController {
    constructor(private readonly sendEmailService: SendEmailService) {}

    @Post()
    @UseInterceptors(FilesInterceptor('file[]'))
    async sendEmail(
        @UploadedFiles() files: Array<Express.Multer.File>,
        @Body() body: any,
        @Body('name') name: string,
        @Body('email') email?: string,
        @Body('phone') tel?: string,
        @Body('message') description?: string
    ) {
        await this.sendEmailService.send(name, tel, files, email, description, JSON.stringify(body, null, 4));

        return { success: true };
    }
}
